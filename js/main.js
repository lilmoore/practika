var lem= new Vue({
    el:"#lem",
    data:{
        products:[
            {id:1,title:"Full-sun",short_text:'Southern growers prefer this variety for winter production.', image:'Full sun.jpg', desc:"Full desc"},
            {id:2,title:"TeddyBear",short_text:'Versatile sunflower with sunny, shaggy blooms for field or container.', image:'Teddybear.jpg', desc:"Full desc"},
            {id:3,title:"Horizon",short_text:'Highly uniform addition to the ProCut® series.', image:'Horizon.jpg', desc:"Full desc"},
            {id:4,title:"Red",short_text:'Flowers are deep-red with brown disks.', image:'Red.jpg', desc:"Full desc"},
            {id:5,title:"Chocolate",short_text:'Good choice for attracting and sustaining bees and other pollinating insects.', image:'Chocolate.jpg', desc:"Full desc"}]
    },
    mounted:function(){
        console.log(window.localStorage.getItem('prod'));
    },
    methods:{
        addItem:function(id){
            window.localStorage.setItem('prod', id);
        }
    }


});